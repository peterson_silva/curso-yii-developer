# Yii Developer Course

[![Build Status](https://github.com/yiisoft/yii/workflows/build/badge.svg)](https://github.com/yiisoft/yii/actions)

PHP application development course with the Yii framework on rental properties.

### Installation

> git clone https://gitlab.com/peterson_silva/curso-yii-developer.git real-state

> cd real-state

> composer install

#### Database

> create database course_yii

In file config/db.php, write:
```php
<?php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=course_yii',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```

with username and password of the database in your PC.

### Migration

> php yii migrate --migrationPath=vendor/webvimark/module-user-management/migrations/

> php yii migrate

and, create table and columns **OR** execute files /sqls/mysql in your database.

### Classes

- yii\behaviors\TimestampBehavior

### Packages

- thtmorais/easyiigii
- kartik-v/yii2-dynagrid


### Dynagrid

Default Configuration:

```php
<?php
    use kartik\dynagrid\DynaGrid;
    
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['class'=>'kartik\grid\CheckboxColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'email:email',
        'phone',
        'address:ntext',
        'renter:boolean',
        'broker:boolean',
        'owner:boolean',
        [
            'class' => 'thtmorais\easyiigii\extensions\ActionColumn',
        ],
    ];
    ?>
    <?= DynaGrid::widget([
        'columns'=>$gridColumn,
        'theme'=>'panel-info',
        'showPersonalize'=>true,
        'storage' => 'session',
        'gridOptions'=>[
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'pjax'=>true,
            'responsiveWrap'=>false,
            'panel'=>[
                'heading'=>'<h3 class="panel-title"><i class="fas fa-user"></i>  '.$this->title.'</h3>',
                'after' => false
            ],
            'toolbar' =>  [
                ['content'=>
                    Html::a('<i class="fas fa-plus"></i>', \yii\helpers\Url::to(['create']),['title'=>'Add', 'class'=>'btn btn-success'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
    ]);
    ?>
```

For icons: https://fontawesome.com/how-to-use/on-the-web/setup/hosting-font-awesome-yourself. Download and extract in the "web/fontawesome" folder

In AppAsset.php, add in

```php
$css = [
    ...,
    'fontawesome/css/all.css',
],
```

### TimestampBehavior

```php
use yii\behaviors\TimestampBehavior;

public function behaviors()
{
    return [
        TimestampBehavior::className(),
    ];
}
```