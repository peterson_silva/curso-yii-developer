create table realty
(
    id          int auto_increment
        primary key,
    description varchar(255) not null,
    value       decimal      not null,
    address     text         null,
    `condition` text         null,
    created_at  int          null,
    updated_at  int          null,
    owner_id    int          not null,
    constraint realty_owner_id_fk
        foreign key (owner_id) references people (id)
);

