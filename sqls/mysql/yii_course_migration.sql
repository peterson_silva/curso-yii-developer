create table migration
(
    version    varchar(180) not null
        primary key,
    apply_time int          null
);

INSERT INTO yii_course.migration (version, apply_time) VALUES ('m000000_000000_base', 1607206876);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m140608_173539_create_user_table', 1607206934);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m140611_133903_init_rbac', 1607206935);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m140808_073114_create_auth_item_group_table', 1607206935);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m140809_072112_insert_superadmin_to_user', 1607206936);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m140809_073114_insert_common_permisison_to_auth_item', 1607206936);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m141023_141535_create_user_visit_log', 1607206936);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m141116_115804_add_bind_to_ip_and_registration_ip_to_user', 1607206936);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m141121_194858_split_browser_and_os_column', 1607206936);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m141201_220516_add_email_and_email_confirmed_to_user', 1607206936);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m141207_001649_create_basic_user_permissions', 1607206937);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m201127_225400_people', 1607206880);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m201127_230023_realty', 1607206880);
INSERT INTO yii_course.migration (version, apply_time) VALUES ('m201127_230034_rental', 1607206880);