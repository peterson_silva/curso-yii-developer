create table auth_assignment
(
    item_name  varchar(64) not null,
    user_id    int         not null,
    created_at int         null,
    primary key (item_name, user_id),
    constraint auth_assignment_ibfk_1
        foreign key (item_name) references auth_item (name)
            on update cascade on delete cascade,
    constraint auth_assignment_ibfk_2
        foreign key (user_id) references user (id)
            on update cascade on delete cascade
)
    charset = utf8;

create index user_id
    on auth_assignment (user_id);

