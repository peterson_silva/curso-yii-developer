create table auth_item_child
(
    parent varchar(64) not null,
    child  varchar(64) not null,
    primary key (parent, child),
    constraint auth_item_child_ibfk_1
        foreign key (parent) references auth_item (name)
            on update cascade on delete cascade,
    constraint auth_item_child_ibfk_2
        foreign key (child) references auth_item (name)
            on update cascade on delete cascade
)
    charset = utf8;

create index child
    on auth_item_child (child);

INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('changeOwnPassword', '/user-management/auth/change-own-password');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('assignRolesToUsers', '/user-management/user-permission/set');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('assignRolesToUsers', '/user-management/user-permission/set-roles');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('editUsers', '/user-management/user/bulk-activate');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('editUsers', '/user-management/user/bulk-deactivate');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('deleteUsers', '/user-management/user/bulk-delete');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('changeUserPassword', '/user-management/user/change-password');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('createUsers', '/user-management/user/create');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('deleteUsers', '/user-management/user/delete');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('viewUsers', '/user-management/user/grid-page-size');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('viewUsers', '/user-management/user/index');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('editUsers', '/user-management/user/update');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('viewUsers', '/user-management/user/view');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('Admin', 'assignRolesToUsers');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('Admin', 'changeOwnPassword');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('Admin', 'changeUserPassword');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('Admin', 'createUsers');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('Admin', 'deleteUsers');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('Admin', 'editUsers');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('editUserEmail', 'viewUserEmail');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('assignRolesToUsers', 'viewUserRoles');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('Admin', 'viewUsers');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('assignRolesToUsers', 'viewUsers');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('changeUserPassword', 'viewUsers');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('createUsers', 'viewUsers');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('deleteUsers', 'viewUsers');
INSERT INTO yii_course.auth_item_child (parent, child) VALUES ('editUsers', 'viewUsers');