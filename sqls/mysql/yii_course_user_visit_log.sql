create table user_visit_log
(
    id         int auto_increment
        primary key,
    token      varchar(255) not null,
    ip         varchar(15)  not null,
    language   char(2)      not null,
    user_agent varchar(255) not null,
    user_id    int          null,
    visit_time int          not null,
    browser    varchar(30)  null,
    os         varchar(20)  null,
    constraint user_visit_log_ibfk_1
        foreign key (user_id) references user (id)
            on update cascade on delete set null
)
    charset = utf8;

create index user_id
    on user_visit_log (user_id);

INSERT INTO yii_course.user_visit_log (id, token, ip, language, user_agent, user_id, visit_time, browser, os) VALUES (1, '5fcc0883e596a', '127.0.0.1', 'pt', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1, 1607207043, 'Chrome', 'Linux');