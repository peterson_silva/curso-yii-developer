create table auth_rule
(
    name       varchar(64) not null
        primary key,
    data       text        null,
    created_at int         null,
    updated_at int         null
)
    charset = utf8;

