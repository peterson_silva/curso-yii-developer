create table auth_item_group
(
    code       varchar(64)  not null
        primary key,
    name       varchar(255) not null,
    created_at int          null,
    updated_at int          null
)
    charset = utf8;

INSERT INTO yii_course.auth_item_group (code, name, created_at, updated_at) VALUES ('userCommonPermissions', 'User common permission', 1607206937, 1607206937);
INSERT INTO yii_course.auth_item_group (code, name, created_at, updated_at) VALUES ('userManagement', 'User management', 1607206937, 1607206937);