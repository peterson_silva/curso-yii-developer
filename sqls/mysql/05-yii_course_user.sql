create table user
(
    id                 int auto_increment
        primary key,
    username           varchar(255)       not null,
    auth_key           varchar(32)        not null,
    password_hash      varchar(255)       not null,
    confirmation_token varchar(255)       null,
    status             int      default 1 not null,
    superadmin         smallint default 0 null,
    created_at         int                not null,
    updated_at         int                not null,
    registration_ip    varchar(15)        null,
    bind_to_ip         varchar(255)       null,
    email              varchar(128)       null,
    email_confirmed    smallint default 0 not null
)
    charset = utf8;

INSERT INTO yii_course.user (id, username, auth_key, password_hash, confirmation_token, status, superadmin, created_at, updated_at, registration_ip, bind_to_ip, email, email_confirmed) VALUES (1, 'superadmin', 'nW24-n3B9gpIeu2w5hOTWvbN64AVH6NF', '$2y$13$/L.36y5Bje1D3jA1hq8sUOzZSejxmSy.tVJh49VPjQzb4YlePOsmm', null, 1, 1, 1607206936, 1607206936, null, null, null, 0);