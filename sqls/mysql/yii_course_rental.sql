create table rental
(
    id            int auto_increment
        primary key,
    description   varchar(255) not null,
    value         decimal      not null,
    terms         text         null,
    date_initial  date         null,
    date_finished date         null,
    created_at    int          null,
    updated_at    int          null,
    broker_id     int          not null,
    renter_id     int          not null,
    constraint realty_broker_id_fk
        foreign key (broker_id) references people (id),
    constraint realty_renter_id_fk
        foreign key (renter_id) references people (id)
);

