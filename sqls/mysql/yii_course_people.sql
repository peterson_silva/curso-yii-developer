create table people
(
    id         int auto_increment
        primary key,
    name       varchar(255)         not null,
    email      varchar(255)         not null,
    phone      varchar(255)         null,
    address    text                 null,
    created_at int                  null,
    updated_at int                  null,
    renter     tinyint(1) default 1 null,
    broker     tinyint(1) default 0 null,
    owner      tinyint(1) default 0 null
);

