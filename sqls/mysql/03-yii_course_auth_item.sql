create table auth_item
(
    name        varchar(64) not null
        primary key,
    type        int         not null,
    description text        null,
    rule_name   varchar(64) null,
    data        text        null,
    created_at  int         null,
    updated_at  int         null,
    group_code  varchar(64) null,
    constraint auth_item_ibfk_1
        foreign key (rule_name) references auth_rule (name)
            on update cascade on delete set null,
    constraint fk_auth_item_group_code
        foreign key (group_code) references auth_item_group (code)
            on update cascade on delete set null
)
    charset = utf8;

create index `idx-auth_item-type`
    on auth_item (type);

create index rule_name
    on auth_item (rule_name);

INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//controller', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//crud', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//easyii-gii-crud', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//easyii-gii-migration', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//easyii-gii-model', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//extension', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//form', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//index', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//model', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//module', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/asset/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/asset/compress', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/asset/template', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/cache/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/cache/flush', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/cache/flush-all', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/cache/flush-schema', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/cache/index', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/fixture/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/fixture/load', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/fixture/unload', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/action', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/diff', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/index', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/preview', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/view', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/hello/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/hello/index', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/help/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/help/index', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/help/list', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/help/list-action-options', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/help/usage', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/message/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/message/config', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/message/config-template', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/message/extract', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/create', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/down', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/fresh', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/history', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/mark', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/new', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/redo', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/to', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/up', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/serve/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/serve/index', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/*', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/change-own-password', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-permission/set', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-permission/set-roles', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/bulk-activate', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/bulk-deactivate', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/bulk-delete', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/change-password', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/create', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/delete', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/grid-page-size', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/index', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/update', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/view', 3, null, null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('Admin', 1, 'Admin', null, null, 1607206937, 1607206937, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('assignRolesToUsers', 2, 'Assign roles to users', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('bindUserToIp', 2, 'Bind user to IP', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('changeOwnPassword', 2, 'Change own password', null, null, 1607206937, 1607206937, 'userCommonPermissions');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('changeUserPassword', 2, 'Change user password', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('commonPermission', 2, 'Common permission', null, null, 1607206936, 1607206936, null);
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('createUsers', 2, 'Create users', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('deleteUsers', 2, 'Delete users', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('editUserEmail', 2, 'Edit user email', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('editUsers', 2, 'Edit users', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('viewRegistrationIp', 2, 'View registration IP', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('viewUserEmail', 2, 'View user email', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('viewUserRoles', 2, 'View user roles', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('viewUsers', 2, 'View users', null, null, 1607206937, 1607206937, 'userManagement');
INSERT INTO yii_course.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('viewVisitLog', 2, 'View visit log', null, null, 1607206937, 1607206937, 'userManagement');