create table auth_rule
(
    name       varchar(64) not null
        constraint auth_rule_pkey
            primary key,
    data       text,
    created_at integer,
    updated_at integer
);

alter table auth_rule
    owner to postgres;

