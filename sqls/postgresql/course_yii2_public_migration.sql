create table migration
(
    version    varchar(180) not null
        constraint migration_pkey
            primary key,
    apply_time integer
);

alter table migration
    owner to postgres;

INSERT INTO public.migration (version, apply_time) VALUES ('m000000_000000_base', 1606519787);
INSERT INTO public.migration (version, apply_time) VALUES ('m201127_225400_people', 1606519799);
INSERT INTO public.migration (version, apply_time) VALUES ('m201127_230023_realty', 1606519799);
INSERT INTO public.migration (version, apply_time) VALUES ('m201127_230034_rental', 1606519799);