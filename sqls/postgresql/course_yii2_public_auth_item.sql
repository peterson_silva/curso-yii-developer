create table auth_item
(
    name        varchar(64) not null
        constraint auth_item_pkey
            primary key,
    type        integer     not null,
    description text,
    rule_name   varchar(64) default NULL::character varying
        constraint auth_item_ibfk_1
            references auth_rule
            on update cascade on delete set null,
    data        text,
    created_at  integer,
    updated_at  integer,
    group_code  varchar(64) default NULL::character varying
        constraint fk_auth_item_group_code
            references auth_item_group
            on update cascade on delete set null
);

alter table auth_item
    owner to postgres;

create index rule_name
    on auth_item (rule_name);

create index idx_auth_item_type
    on auth_item (type);

create index fk_auth_item_group_code
    on auth_item (group_code);

INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//controller', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//crud', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//extension', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//form', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//index', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//model', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('//module', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/asset/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/asset/compress', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/asset/template', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/cache/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/cache/flush', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/cache/flush-all', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/cache/flush-schema', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/cache/index', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/fixture/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/fixture/load', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/fixture/unload', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/action', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/diff', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/index', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/preview', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/gii/default/view', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/hello/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/hello/index', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/help/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/help/index', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/message/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/message/config', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/message/extract', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/create', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/down', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/history', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/mark', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/new', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/redo', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/to', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/migrate/up', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/bulk-activate', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/bulk-deactivate', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/bulk-delete', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/create', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/delete', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/grid-page-size', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/grid-sort', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/index', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/toggle-attribute', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/update', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth-item-group/view', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/captcha', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/change-own-password', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/confirm-email', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/confirm-email-receive', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/confirm-registration-email', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/login', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/logout', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/password-recovery', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/password-recovery-receive', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/auth/registration', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/bulk-activate', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/bulk-deactivate', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/bulk-delete', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/create', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/delete', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/grid-page-size', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/grid-sort', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/index', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/refresh-routes', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/set-child-permissions', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/set-child-routes', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/toggle-attribute', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/update', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/permission/view', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/bulk-activate', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/bulk-deactivate', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/bulk-delete', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/create', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/delete', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/grid-page-size', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/grid-sort', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/index', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/set-child-permissions', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/set-child-roles', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/toggle-attribute', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/update', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/role/view', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-permission/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-permission/set', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-permission/set-roles', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/bulk-activate', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/bulk-deactivate', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/bulk-delete', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/create', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/delete', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/grid-page-size', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/grid-sort', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/index', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/toggle-attribute', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/update', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user-visit-log/view', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/*', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/bulk-activate', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/bulk-deactivate', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/bulk-delete', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/change-password', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/create', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/delete', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/grid-page-size', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/grid-sort', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/index', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/toggle-attribute', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/update', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('/user-management/user/view', 3, null, null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('Admin', 1, 'Admin', null, null, 1426062189, 1426062189, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('assignRolesToUsers', 2, 'Assign roles to users', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('bindUserToIp', 2, 'Bind user to IP', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('changeOwnPassword', 2, 'Change own password', null, null, 1426062189, 1426062189, 'userCommonPermissions');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('changeUserPassword', 2, 'Change user password', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('commonPermission', 2, 'Common permission', null, null, 1426062188, 1426062188, null);
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('createUsers', 2, 'Create users', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('deleteUsers', 2, 'Delete users', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('editUserEmail', 2, 'Edit user email', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('editUsers', 2, 'Edit users', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('viewRegistrationIp', 2, 'View registration IP', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('viewUserEmail', 2, 'View user email', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('viewUserRoles', 2, 'View user roles', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('viewUsers', 2, 'View users', null, null, 1426062189, 1426062189, 'userManagement');
INSERT INTO public.auth_item (name, type, description, rule_name, data, created_at, updated_at, group_code) VALUES ('viewVisitLog', 2, 'View visit log', null, null, 1426062189, 1426062189, 'userManagement');