create table "user"
(
    id                 integer      default nextval('user_seq'::regclass) not null
        constraint user_pkey
            primary key,
    username           varchar(255)                                       not null,
    auth_key           varchar(32)                                        not null,
    password_hash      varchar(255)                                       not null,
    confirmation_token varchar(255) default NULL::character varying,
    status             integer      default 1                             not null,
    superadmin         smallint     default '0'::smallint,
    created_at         integer                                            not null,
    updated_at         integer                                            not null,
    registration_ip    varchar(15)  default NULL::character varying,
    bind_to_ip         varchar(255) default NULL::character varying,
    email              varchar(128) default NULL::character varying,
    email_confirmed    smallint     default '0'::smallint                 not null
);

alter table "user"
    owner to postgres;

INSERT INTO public."user" (id, username, auth_key, password_hash, confirmation_token, status, superadmin, created_at, updated_at, registration_ip, bind_to_ip, email, email_confirmed) VALUES (1, 'admin', 'kz2px152FAWlkHbkZoCiXgBAd-S8SSjF', '$2y$13$6Usx8yH666k6t5dc0mGMhOZRVN182F1A2nJRnC3ZAG5YXpFrH6l1y', null, 1, 1, 1426062188, 1426062188, null, null, null, 0);