create table auth_item_child
(
    parent varchar(64) not null
        constraint auth_item_child_ibfk_1
            references auth_item
            on update cascade on delete cascade,
    child  varchar(64) not null
        constraint auth_item_child_ibfk_2
            references auth_item
            on update cascade on delete cascade,
    constraint auth_item_child_pkey
        primary key (parent, child)
);

alter table auth_item_child
    owner to postgres;

create index child
    on auth_item_child (child);

INSERT INTO public.auth_item_child (parent, child) VALUES ('changeOwnPassword', '/user-management/auth/change-own-password');
INSERT INTO public.auth_item_child (parent, child) VALUES ('assignRolesToUsers', '/user-management/user-permission/set');
INSERT INTO public.auth_item_child (parent, child) VALUES ('assignRolesToUsers', '/user-management/user-permission/set-roles');
INSERT INTO public.auth_item_child (parent, child) VALUES ('viewVisitLog', '/user-management/user-visit-log/grid-page-size');
INSERT INTO public.auth_item_child (parent, child) VALUES ('viewVisitLog', '/user-management/user-visit-log/index');
INSERT INTO public.auth_item_child (parent, child) VALUES ('viewVisitLog', '/user-management/user-visit-log/view');
INSERT INTO public.auth_item_child (parent, child) VALUES ('editUsers', '/user-management/user/bulk-activate');
INSERT INTO public.auth_item_child (parent, child) VALUES ('editUsers', '/user-management/user/bulk-deactivate');
INSERT INTO public.auth_item_child (parent, child) VALUES ('deleteUsers', '/user-management/user/bulk-delete');
INSERT INTO public.auth_item_child (parent, child) VALUES ('changeUserPassword', '/user-management/user/change-password');
INSERT INTO public.auth_item_child (parent, child) VALUES ('createUsers', '/user-management/user/create');
INSERT INTO public.auth_item_child (parent, child) VALUES ('deleteUsers', '/user-management/user/delete');
INSERT INTO public.auth_item_child (parent, child) VALUES ('viewUsers', '/user-management/user/grid-page-size');
INSERT INTO public.auth_item_child (parent, child) VALUES ('viewUsers', '/user-management/user/index');
INSERT INTO public.auth_item_child (parent, child) VALUES ('editUsers', '/user-management/user/update');
INSERT INTO public.auth_item_child (parent, child) VALUES ('viewUsers', '/user-management/user/view');
INSERT INTO public.auth_item_child (parent, child) VALUES ('Admin', 'assignRolesToUsers');
INSERT INTO public.auth_item_child (parent, child) VALUES ('Admin', 'changeOwnPassword');
INSERT INTO public.auth_item_child (parent, child) VALUES ('Admin', 'changeUserPassword');
INSERT INTO public.auth_item_child (parent, child) VALUES ('Admin', 'createUsers');
INSERT INTO public.auth_item_child (parent, child) VALUES ('Admin', 'deleteUsers');
INSERT INTO public.auth_item_child (parent, child) VALUES ('Admin', 'editUsers');
INSERT INTO public.auth_item_child (parent, child) VALUES ('editUserEmail', 'viewUserEmail');
INSERT INTO public.auth_item_child (parent, child) VALUES ('assignRolesToUsers', 'viewUserRoles');
INSERT INTO public.auth_item_child (parent, child) VALUES ('Admin', 'viewUsers');
INSERT INTO public.auth_item_child (parent, child) VALUES ('assignRolesToUsers', 'viewUsers');
INSERT INTO public.auth_item_child (parent, child) VALUES ('changeUserPassword', 'viewUsers');
INSERT INTO public.auth_item_child (parent, child) VALUES ('createUsers', 'viewUsers');
INSERT INTO public.auth_item_child (parent, child) VALUES ('deleteUsers', 'viewUsers');
INSERT INTO public.auth_item_child (parent, child) VALUES ('editUsers', 'viewUsers');