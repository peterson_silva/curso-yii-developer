create table user_visit_log
(
    id         integer     default nextval('user_visit_log_seq'::regclass) not null
        constraint user_visit_log_pkey
            primary key,
    token      varchar(255)                                                not null,
    ip         varchar(15)                                                 not null,
    language   char(2)                                                     not null,
    user_agent varchar(255)                                                not null,
    user_id    integer
        constraint user_visit_log_ibfk_1
            references "user"
            on update cascade on delete set null,
    visit_time integer                                                     not null,
    browser    varchar(30) default NULL::character varying,
    os         varchar(20) default NULL::character varying
);

alter table user_visit_log
    owner to postgres;

create index user_id
    on user_visit_log (user_id);

INSERT INTO public.user_visit_log (id, token, ip, language, user_agent, user_id, visit_time, browser, os) VALUES (1, '5fc19ec018918', '127.0.0.1', 'pt', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 1, 1606524608, 'Chrome', 'Linux');