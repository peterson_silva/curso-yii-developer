create table realty
(
    id          serial       not null
        constraint realty_pkey
            primary key,
    description varchar(255) not null,
    value       numeric(10)  not null,
    address     text,
    condition   text,
    created_at  integer,
    updated_at  integer,
    owner_id    integer      not null
        constraint realty_owner_id_fk
            references people
);

alter table realty
    owner to postgres;

