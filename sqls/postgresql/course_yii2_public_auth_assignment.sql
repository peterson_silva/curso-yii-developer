create table auth_assignment
(
    item_name  varchar(64) not null
        constraint auth_assignment_ibfk_1
            references auth_item
            on update cascade on delete cascade,
    user_id    integer     not null
        constraint auth_assignment_ibfk_2
            references "user"
            on update cascade on delete cascade,
    created_at integer,
    constraint auth_assignment_pkey
        primary key (item_name, user_id)
);

alter table auth_assignment
    owner to postgres;

create index user_id4
    on auth_assignment (user_id);

