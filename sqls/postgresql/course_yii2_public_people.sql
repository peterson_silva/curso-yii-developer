create table people
(
    id         serial       not null
        constraint people_pkey
            primary key,
    name       varchar(255) not null,
    email      varchar(255) not null,
    phone      varchar(255),
    address    text,
    created_at integer,
    updated_at integer,
    renter     boolean default true,
    broker     boolean default false,
    owner      boolean default false
);

alter table people
    owner to postgres;

INSERT INTO public.people (id, name, email, phone, address, created_at, updated_at, renter, broker, owner) VALUES (1, 'f gdfgdf gd gdf fg ', 'dfg df', 'dfgdfg df', 'd fgdf gd', 54, 4564, true, false, false);