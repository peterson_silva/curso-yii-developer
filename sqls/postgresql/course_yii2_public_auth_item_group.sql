create table auth_item_group
(
    code       varchar(64)  not null
        constraint auth_item_group_pkey
            primary key,
    name       varchar(255) not null,
    created_at integer,
    updated_at integer
);

alter table auth_item_group
    owner to postgres;

INSERT INTO public.auth_item_group (code, name, created_at, updated_at) VALUES ('userCommonPermissions', 'User common permission', 1426062189, 1426062189);
INSERT INTO public.auth_item_group (code, name, created_at, updated_at) VALUES ('userManagement', 'User management', 1426062189, 1426062189);