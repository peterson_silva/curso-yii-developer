create table rental
(
    id            serial       not null
        constraint rental_pkey
            primary key,
    description   varchar(255) not null,
    value         numeric(10)  not null,
    terms         text,
    date_initial  date,
    date_finished date,
    created_at    integer,
    updated_at    integer,
    broker_id     integer      not null
        constraint realty_broker_id_fk
            references people,
    renter_id     integer      not null
        constraint realty_renter_id_fk
            references people
);

alter table rental
    owner to postgres;

