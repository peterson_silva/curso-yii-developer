<?php

namespace app\models;

use Yii;

/**
 * This is the base model class for table "rental".
 *
 * @property integer $id
 * @property string $description
 * @property string $value
 * @property string $terms
 * @property string $date_initial
 * @property string $date_finished
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $broker_id
 * @property integer $renter_id
 *
 * @property \app\models\People $broker
 * @property \app\models\People $renter
 */
class Rental extends \yii\db\ActiveRecord
{

    public function behaviors(){
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'value', 'broker_id', 'renter_id'], 'required'],
            [['value'], 'number'],
            [['terms'], 'string'],
            [['date_initial', 'date_finished'], 'safe'],
            [['created_at', 'updated_at', 'broker_id', 'renter_id'], 'default', 'value' => null],
            [['created_at', 'updated_at', 'broker_id', 'renter_id'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => People::className(), 'targetAttribute' => ['broker_id' => 'id']],
            [['renter_id'], 'exist', 'skipOnError' => true, 'targetClass' => People::className(), 'targetAttribute' => ['renter_id' => 'id']],        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rental';
    }

    /**
    * @inheritdoc
    */
    public static function representingColumn()
    {
        return 'terms';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'value' => 'Value',
            'terms' => 'Terms',
            'date_initial' => 'Date Initial',
            'date_finished' => 'Date Finished',
            'broker_id' => 'Broker ID',
            'renter_id' => 'Renter ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(\app\models\People::className(), ['id' => 'broker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRenter()
    {
        return $this->hasOne(\app\models\People::className(), ['id' => 'renter_id']);
    }
}
