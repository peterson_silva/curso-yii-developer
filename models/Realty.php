<?php

namespace app\models;

use Yii;

/**
 * This is the base model class for table "realty".
 *
 * @property integer $id
 * @property string $description
 * @property string $value
 * @property string $address
 * @property string $condition
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $owner_id
 *
 * @property \app\models\People $owner
 */
class Realty extends \yii\db\ActiveRecord
{

    public function behaviors(){
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'value', 'owner_id'], 'required'],
            [['value'], 'number'],
            [['address', 'condition'], 'string'],
            [['created_at', 'updated_at', 'owner_id'], 'default', 'value' => null],
            [['created_at', 'updated_at', 'owner_id'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => People::className(), 'targetAttribute' => ['owner_id' => 'id']],        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'realty';
    }

    /**
    * @inheritdoc
    */
    public static function representingColumn()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'value' => 'Value',
            'address' => 'Address',
            'condition' => 'Condition',
            'owner_id' => 'Owner ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(\app\models\People::className(), ['id' => 'owner_id']);
    }
}
