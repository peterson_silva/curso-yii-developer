<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rental;

/**
 * app\models\RentalSearch represents the model behind the search form about `app\models\Rental`.
 */
 class RentalSearch extends Rental
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'broker_id', 'renter_id'], 'integer'],
            [['description', 'terms', 'date_initial', 'date_finished'], 'safe'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rental::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes'=>[
                'broker_id'=>[
                    'asc'=>['people'=>SORT_ASC],
                    'desc'=>['people'=>SORT_DESC],
                ],
                'renter_id'=>[
                    'asc'=>['people'=>SORT_ASC],
                    'desc'=>['people'=>SORT_DESC],
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'value' => $this->value,
            'date_initial' => $this->date_initial,
            'date_finished' => $this->date_finished,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'broker_id' => $this->broker_id,
            'renter_id' => $this->renter_id,
        ]);

        $query->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'terms', $this->terms]);

        return $dataProvider;
    }
}
