<?php

namespace app\models;

use Yii;

/**
 * This is the base model class for table "people".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property integer $created_at
 * @property integer $updated_at
 * @property boolean $renter
 * @property boolean $broker
 * @property boolean $owner
 *
 * @property \app\models\Realty[] $realties
 * @property \app\models\Rental[] $rentals
 */
class People extends \yii\db\ActiveRecord
{

    public function behaviors(){
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['address'], 'string'],
            [['created_at', 'updated_at'], 'default', 'value' => null],
            [['created_at', 'updated_at'], 'integer'],
            [['renter', 'broker', 'owner'], 'boolean'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],            
            [['email'],'email'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'people';
    }

    /**
    * @inheritdoc
    */
    public static function representingColumn()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'renter' => 'Renter',
            'broker' => 'Broker',
            'owner' => 'Owner',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealties()
    {
        return $this->hasMany(\app\models\Realty::className(), ['owner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentals()
    {
        return $this->hasMany(\app\models\Rental::className(), ['renter_id' => 'id']);
    }
}
