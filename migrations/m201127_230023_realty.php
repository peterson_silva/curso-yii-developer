<?php

use yii\db\Migration;

/**
 * Class m201127_230023_realty
 */
class m201127_230023_realty extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('realty', [
            'id'=>$this->primaryKey(),
            'description'=>$this->string()->notNull(),
            'value'=>$this->decimal()->notNull(),
            'address'=>$this->text(),
            'condition'=>$this->text(),

            'created_at'=>$this->integer(),
            'updated_at'=>$this->integer(),

            'owner_id'=>$this->integer()->notNull()
        ]);

        $this->addForeignKey('realty_owner_id_fk', 'realty', 'owner_id','people', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('realty');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201127_230023_realty cannot be reverted.\n";

        return false;
    }
    */
}
