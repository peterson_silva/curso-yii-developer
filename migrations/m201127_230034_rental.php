<?php

use yii\db\Migration;

/**
 * Class m201127_230034_rental
 */
class m201127_230034_rental extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rental', [
            'id' => $this->primaryKey(),
            'description' => $this->string()->notNull(),
            'value' => $this->decimal()->notNull(),
            'terms' => $this->text(),
            'date_initial' => $this->date(),
            'date_finished' => $this->date(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),

            'broker_id' => $this->integer()->notNull(),
            'renter_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('realty_broker_id_fk', 'rental', 'broker_id', 'people', 'id');
        $this->addForeignKey('realty_renter_id_fk', 'rental', 'renter_id', 'people', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rental');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201127_230034_rental cannot be reverted.\n";

        return false;
    }
    */
}
