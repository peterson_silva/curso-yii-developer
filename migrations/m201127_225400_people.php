<?php

use yii\db\Migration;

/**
 * Class m201127_225400_people
 */
class m201127_225400_people extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('people', [
            'id'=>$this->primaryKey(),
            'name'=>$this->string()->notNull(),
            'email'=>$this->string()->notNull(),
            'phone'=>$this->string(),
            'address'=>$this->text(),

            'created_at'=>$this->integer(),
            'updated_at'=>$this->integer(),

            'renter'=>$this->boolean()->defaultValue(true),
            'broker'=>$this->boolean()->defaultValue(false),
            'owner'=>$this->boolean()->defaultValue(false),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('people');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201127_225400_people cannot be reverted.\n";

        return false;
    }
    */
}
